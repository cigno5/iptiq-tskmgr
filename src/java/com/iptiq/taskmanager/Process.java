package com.iptiq.taskmanager;

import java.util.Date;

public interface Process {

    enum Priority { low, medium, high};

    Long getId();
    Priority getPriority();
    Date getTimestamp();

    void kill();

}
