package com.iptiq.taskmanager.impl;

import com.iptiq.taskmanager.CapacityException;
import com.iptiq.taskmanager.Process;
import com.iptiq.taskmanager.ProcessListManager;

import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Stream;

public class PriorityProcessListManager implements ProcessListManager {
    private final int capacity;
    private final TreeSet<Process> set;

    public PriorityProcessListManager(int capacity) {
        this.capacity = capacity;
        //the set is sorted by priority ascending and timestamp descending. Head (first element) is the newest with the highest priority, tail (last element) is the oldest
        //with lower priority
        this.set = new TreeSet<>(Comparator
                .comparing(Process::getPriority)
                .reversed()
                .thenComparing(Process::getTimestamp));
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void add(Process process) throws CapacityException {
        synchronized (set) {
            if (set.size() == capacity) {
                //if priority of current process is higher than the highest priority in the queue, then it suggests removing the last element (oldest with the lowest priority)
                if (process.getPriority().compareTo(set.first().getPriority()) > 0) {
                    throw new CapacityException(set.last());
                } else {
                    //otherwise, just skip
                    return;
                }
            }
            set.add(process);
        }
    }

    @Override
    public void remove(Process process) {
        synchronized (set) {
            set.remove(process);
        }
    }

    @Override
    public Stream<Process> processStream() {
        return set.stream();
    }
}
