package com.iptiq.taskmanager.impl;

import com.iptiq.taskmanager.CapacityException;
import com.iptiq.taskmanager.Process;
import com.iptiq.taskmanager.ProcessListManager;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Stream;

public class FIFOProcessListManager implements ProcessListManager {
    private final int capacity;
    private final ConcurrentLinkedQueue<Process> queue;

    public FIFOProcessListManager(int capacity) {
        this.capacity = capacity;
        this.queue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void add(Process process) throws CapacityException {
        synchronized (queue) {
            if (queue.size() == capacity) {
                // the expendable is the head of the queue, which is the oldest item
                throw new CapacityException(queue.peek());
            }
            queue.add(process);
        }
    }

    @Override
    public void remove(Process process) {
        synchronized (queue) {
            queue.remove(process);
        }
    }

    @Override
    public Stream<Process> processStream() {
        return queue.stream();
    }
}
