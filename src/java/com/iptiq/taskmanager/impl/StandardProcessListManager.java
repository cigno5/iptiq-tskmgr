package com.iptiq.taskmanager.impl;

import com.iptiq.taskmanager.CapacityException;
import com.iptiq.taskmanager.Process;
import com.iptiq.taskmanager.ProcessListManager;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Stream;

public class StandardProcessListManager implements ProcessListManager {
    private final CopyOnWriteArrayList<Process> list;
    private final int capacity;

    public StandardProcessListManager(int capacity) {
        this.capacity = capacity;
        this.list = new CopyOnWriteArrayList<>();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void add(Process process) throws CapacityException {
        synchronized (list) {
            if (list.size() == capacity) {
                throw new CapacityException();
            }

            list.add(process);
        }
    }

    @Override
    public void remove(Process process) {
        synchronized (list) {
            list.remove(process);
        }
    }

    @Override
    public Stream<Process> processStream() {
        return list.stream();
    }
}
