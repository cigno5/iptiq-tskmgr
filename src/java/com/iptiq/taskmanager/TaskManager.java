package com.iptiq.taskmanager;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskManager {
    private final ProcessListManager listManager;
    private final ExecutorService executorService;

    public TaskManager(ProcessListManager listManager) {
        this.listManager = listManager;
        this.executorService = Executors.newFixedThreadPool(listManager.getCapacity());
    }

    public synchronized void addProcess(Process process) throws CapacityException {
        try {
            this.listManager.add(process);
        } catch (CapacityException e) {
            if (e.roomAvailable()) {
                //if space can be retrieved it reads
                killProcess(e.getExpendableProcess());
                this.listManager.add(process);
            } else {
                throw e;
            }
        }
    }

    public List<Process> listByTimestamp() {
        return list(Comparator.comparing(Process::getTimestamp).reversed());
    }

    public List<Process> listByPriority() {
        return list(Comparator.comparing(Process::getPriority).reversed());
    }

    public List<Process> listById() {
        return list(Comparator.comparing(Process::getId).reversed());
    }

    public void killAll() {
        kill(x -> true);
    }

    public void kill(Long processId) {
        kill(p -> Objects.equals(p.getId(), processId));
    }

    public void killAll(Process.Priority priority) {
        kill(p -> Objects.equals(p.getPriority(), priority));
    }

    boolean shutdown() throws InterruptedException {
        executorService.shutdown();
        return executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    List<Process> list() {
        return listManager.processStream().collect(Collectors.toList());
    }

    Stream<Process> stream() {
        return listManager.processStream();
    }

    private void kill(Predicate<Process> filter) {
        consumeProcesses(filter, this::killProcess);
    }

    private void consumeProcesses(Predicate<Process> filter, Consumer<Process> consumer) {
        listManager.processStream()
                .filter(filter)
                .forEach(process -> executorService.submit(() -> consumer.accept(process)));
    }

    private List<Process> list(Comparator<Process> c) {
        return listManager.processStream().sorted(c).collect(Collectors.toList());
    }

    private void killProcess(Process process) {
        process.kill();
        listManager.remove(process);
    }
}
