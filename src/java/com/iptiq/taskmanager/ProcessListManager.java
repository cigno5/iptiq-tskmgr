package com.iptiq.taskmanager;

import java.util.stream.Stream;

public interface ProcessListManager {

    int getCapacity();

    void add(Process process) throws CapacityException;

    void remove(Process process);

    Stream<Process> processStream();

}
