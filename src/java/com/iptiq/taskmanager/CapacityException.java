package com.iptiq.taskmanager;

public class CapacityException extends Exception {
    private final Process expendableProcess;

    public CapacityException() {
        this(null);
    }

    public CapacityException(Process expendableProcess) {
        this.expendableProcess = expendableProcess;
    }

    public boolean roomAvailable() {
        return expendableProcess != null;
    }

    public Process getExpendableProcess() {
        return expendableProcess;
    }

}
