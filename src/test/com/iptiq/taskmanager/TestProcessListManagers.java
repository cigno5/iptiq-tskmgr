package com.iptiq.taskmanager;

import com.iptiq.taskmanager.impl.FIFOProcessListManager;
import com.iptiq.taskmanager.impl.PriorityProcessListManager;
import com.iptiq.taskmanager.impl.StandardProcessListManager;
import com.iptiq.taskmanager.test.MyAppProcess;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class TestProcessListManagers {
    private static final Random RANDOM = new Random();
    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    @Test
    public void testStandardProcessListManager() throws CapacityException, InterruptedException {
        TaskManager tskMgr = new TaskManager(new StandardProcessListManager(5));

        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        //ok so far, now exception
        Assert.assertThrows(CapacityException.class, () -> tskMgr.addProcess(spawnNewProcess()));

        //clear
        tskMgr.killAll();

        //and add with exception (presumably no process has been killed yet)
        Assert.assertThrows(CapacityException.class, () -> tskMgr.addProcess(spawnNewProcess()));

        Thread.sleep(5000);
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());

        tskMgr.shutdown();
    }

    @Test
    public void testFIFOProcessListManager() throws CapacityException, InterruptedException {
        TaskManager tskMgr = new TaskManager(new FIFOProcessListManager(5));

        Process firstProcess;
        tskMgr.addProcess(firstProcess = spawnNewProcess());
        Process secondProcess;
        tskMgr.addProcess(secondProcess = spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        tskMgr.addProcess(spawnNewProcess());
        //ok so far, now exception
        tskMgr.list().forEach(System.out::println);

        //adding another one, the first one should be lost
        tskMgr.addProcess(spawnNewProcess());
        Thread.sleep(5500);
        Assert.assertNotEquals(firstProcess, tskMgr.list().get(0));
        Assert.assertEquals(secondProcess, tskMgr.list().get(0));

        //adding another one, the first one should be lost
        tskMgr.addProcess(spawnNewProcess());
        Thread.sleep(5500);
        Assert.assertNotEquals(secondProcess, tskMgr.list().get(0));
    }

    @Test
    public void testPriorityProcessListManager() throws CapacityException, InterruptedException {
        TaskManager tskMgr = new TaskManager(new PriorityProcessListManager(5));

        tskMgr.addProcess(spawnNewProcess(Process.Priority.medium));
        tskMgr.addProcess(spawnNewProcess(Process.Priority.low));
        tskMgr.addProcess(spawnNewProcess(Process.Priority.medium));
        tskMgr.addProcess(spawnNewProcess(Process.Priority.low));
        tskMgr.addProcess(spawnNewProcess(Process.Priority.medium));

        //situation snapshot
        List<Long> snapshot = tskMgr.stream().map(Process::getId).collect(Collectors.toList());

        //add a process with medium priority --> already present, skipped without notice
        tskMgr.addProcess(spawnNewProcess(Process.Priority.medium));
        Thread.sleep(5500);
        List<Long> snapshot2 = tskMgr.stream().map(Process::getId).collect(Collectors.toList());
        Assert.assertEquals(snapshot, snapshot2);

        //add a process with high priority --> last one should have been removed
        tskMgr.addProcess(spawnNewProcess(Process.Priority.high));
        Thread.sleep(5500);
        snapshot2 = tskMgr.stream().map(Process::getId).collect(Collectors.toList());
        Assert.assertNotEquals(snapshot, snapshot2);
    }

    private static Process spawnNewProcess() throws InterruptedException {
        return spawnNewProcess(Process.Priority.values()[RANDOM.nextInt(3)]);
    }

    private static Process spawnNewProcess(Process.Priority priority) throws InterruptedException {
        //if too fast all timestamps are the same
        Thread.sleep(RANDOM.nextInt(500));
        return new MyAppProcess(
                ID_GENERATOR.incrementAndGet(),
                priority,
                2000 + RANDOM.nextInt(3) * 1000);

    }

}
