package com.iptiq.taskmanager.test;

import com.iptiq.taskmanager.Process;

import java.util.Date;
import java.util.Objects;

public class MyAppProcess implements Process {
    private final Long id;
    private final Priority priority;
    private final Date timestamp;
    private final long killWait;

    public MyAppProcess(Long id, Priority priority, long killWait) {
        this.id = id;
        this.priority = priority;
        this.timestamp = new Date();
        this.killWait = killWait;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public void kill() {
        try {
            System.out.println(this.toString() + " dying in " + killWait + " millis ...");
            Thread.sleep(killWait);
            System.out.println("..." + this.toString() + " dead.");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Process{" + timestamp.getTime() + " - " + id + " (" + priority + ")}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyAppProcess that = (MyAppProcess) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
