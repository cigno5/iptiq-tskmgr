# iptiq-tskmgr


## Rationale
Main idea behind the design is to separate the task manager itself from the actual implementation of the 
list of the process. 

Therefore, the class `TaskManager` will accept an implementation of the interface `ProcessListManager` which 
will implement the desired algorithm wanted by the customer. 

On this rationale the `TaskManager` is the one that knows how to manage processes, and it uses the basic services offered by the 
`ProcessListManager` to do the job. On the other hand `ProcessListManager` is only responsible to the coherency of the list
of the processes it manages, and for example, it knows how to remove a process but not how to kill it (which is role of the task
manager)

`TaskManager` knows how to kill processes, and when they're dead it removes them from the list. This is done in an asynchronous way
because a process could take longer to be closed. 

Each implementation of `ProcessListManager` in the `add` method checks the capacity and, if it has been reached, throws a 
`CapacityException` informing, if needed, the expendable process to be killed & removed to make room for the new one. The `TaskManager`
ensures that the expendable process is killed and removed and attempt again to add the process to the list, assumed now there is capacity enough.

Each implementation of `ProcessListManager` tends to use a thread-safe collection implementation, but the capacity is not managed by
the collections themselves and therefore the synchronized blocks in `add` and `remove` method ensure the consistency.

`TaskManager.add` method is synchronized to ensure that multiple threads, when the capacity is reached, are enqueued for the 
operation of adding a process. Being the operation of try-to-add/kill&remove/add-again in the same block there is no way some clients 
can fill in a position in the middle of another `add` operation.

## Tests
I created some tests with JUni4 to verify that all implementations are able to manage properly capacity.
